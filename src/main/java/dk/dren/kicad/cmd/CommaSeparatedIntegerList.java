package dk.dren.kicad.cmd;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommaSeparatedIntegerList {
    @Getter
    private final List<Integer> ints = new ArrayList<>();

    public CommaSeparatedIntegerList() {
    }

    public CommaSeparatedIntegerList(String str) {
        add(str);
    }

    public boolean add(String str) {
        Arrays.stream(str.split(",")).forEach(part->{
            try {
                ints.add(Integer.valueOf(part));
            } catch (Exception e) {
                throw new IllegalArgumentException("Could not parse '"+part+"' as an integer", e);
            }
        });
        return true;
    }


    public int size() {
        return ints.size();
    }
}
