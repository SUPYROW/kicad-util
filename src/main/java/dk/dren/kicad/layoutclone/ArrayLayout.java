package dk.dren.kicad.layoutclone;

import dk.dren.kicad.primitive.XYR;
import lombok.Getter;
import org.decimal4j.immutable.Decimal6f;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Very simple layout that allows generation of an array with groups that are either manually or automatically ordered.
 */
@Getter
public class ArrayLayout implements Layout {
    private final List<LayoutOffset> offsets;

    /**
     *
     * @param referencePitch The difference between the template module references and the slave groups.
     * @param count The number of slave groups
     * @param xPitch The distance between the template and the first group and each group
     * @param xCount The number of groups per line
     * @param yPitch The distance between lines.
     * @return The layout
     */
    public static ArrayLayout simpleGrid(int referencePitch, int count, Decimal6f xPitch, int xCount, Decimal6f yPitch) {
        if (referencePitch != 100 && referencePitch != 1000) {
            throw new IllegalArgumentException("The offset pitch should be either either 100 or 1000, found "+referencePitch);
        }

        List<Integer> referenceOffsets = new ArrayList<>();
        int referenceOffset = referencePitch;
        for (int i=0;i<count;i++) {
            referenceOffsets.add(referenceOffset);
            referenceOffset += referencePitch;
        }

        return new ArrayLayout(referenceOffsets, xPitch, xCount, yPitch);
    }

    /**
     * Creates an single line layout with reference offsets being manually specified
     *
     * @param referenceOffsets The list of offsets from the template for the target
     * @param xPitch The distance in the x direction between groups
     */
    public ArrayLayout(List<Integer> referenceOffsets, Decimal6f xPitch) {
       this(referenceOffsets, xPitch, Integer.MAX_VALUE, Decimal6f.ZERO);
    }

    /**
     * Creates an array layout
     *
     * @param referenceOffsets The list of offsets from the template for the target
     * @param xPitch The distance in the x direction between groups
     * @param xCount The number of groups per line
     * @param yPitch The distance between lines
     */
    public ArrayLayout(List<Integer> referenceOffsets, Decimal6f xPitch, int xCount, Decimal6f yPitch) {
        int xc = 0;
        int yc = 0;

        Set<Integer> seen = new TreeSet<>();
        offsets = new ArrayList<>();
        for (Integer referenceOffset : referenceOffsets) {
            if (referenceOffset == 0) {
                throw new IllegalArgumentException("The offset in reference should not be zero");
            }
            if (referenceOffset % 100 != 0 && referenceOffset % 1000 != 0) {
                throw new IllegalArgumentException("The offset in reference should be a multiple of either 100 or 1000, found "+referenceOffset);
            }
            if (seen.contains(referenceOffset)) {
                throw new IllegalArgumentException("Duplicate reference offset: "+referenceOffset);
            }
            seen.add(referenceOffset);

            XYR offset = new XYR(xPitch.multiply(xc+1), yPitch.multiply(yc), Decimal6f.ZERO);

            offsets.add(new LayoutOffset(offset, referenceOffset));

            if (++xc >= xCount) {
                xc = 0;
                yc++;
            }
        }

    }
}
