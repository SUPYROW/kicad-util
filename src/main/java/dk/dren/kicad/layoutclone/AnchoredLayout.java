package dk.dren.kicad.layoutclone;

import dk.dren.kicad.pcb.Board;
import dk.dren.kicad.pcb.Module;
import dk.dren.kicad.primitive.ModuleReference;
import dk.dren.kicad.primitive.XYR;
import lombok.Getter;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Calculates offsets between already placed anchor components, the idea is that the
 * components referenced in the anchorReferences set are pre-placed by the user to indicate the
 * placement of each group, the other components are placed around the anchor component of each group.
 *
 * This allows non-uniform placement of groups, like staggering or following a curve.
 */
@Getter
public class AnchoredLayout implements Layout {
    private final List<LayoutOffset> offsets;

    public AnchoredLayout(Board board, List<Module> templateModules, Set<ModuleReference> anchorReferences) {
        Optional<Module> templateAnchor = templateModules.stream()
                .filter(m -> anchorReferences.contains(m.getReference()))
                .findFirst();
        if (!templateAnchor.isPresent()) {
            throw new IllegalArgumentException("One of the anchor references needs to be in the anchorReferences set");
        }
        offsets = board.modules()
                .filter(m->anchorReferences.contains(m.getReference()))
                .filter(m->!m.getReference().equals(templateAnchor.get().getReference()))
                .map(m->offset(templateAnchor.get(), m))
                .collect(Collectors.toList());
    }

    private LayoutOffset offset(Module anchor, Module module) {
        XYR posDiff = anchor.getPosition().getXyr().diff(module.getPosition().getXyr());
        int refDiff = anchor.getReference().diff(module.getReference());

        if (refDiff % 100 != 0 && refDiff % 1000 != 0) {
            throw new IllegalArgumentException("The offset in reference between "+anchor+" and "+module+" should be a multiple of either 100 or 1000");
        }

        return new LayoutOffset(posDiff, refDiff);
    }


}
