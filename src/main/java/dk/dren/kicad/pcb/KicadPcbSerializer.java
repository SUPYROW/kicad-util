package dk.dren.kicad.pcb;

import de.tudresden.inf.lat.jsexp.Sexp;
import de.tudresden.inf.lat.jsexp.SexpFactory;
import de.tudresden.inf.lat.jsexp.SexpParserException;
import de.tudresden.inf.lat.jsexp.SexpString;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class KicadPcbSerializer {

    public static Board read(File kicadPcbFile) {
        try (InputStreamReader fileReader = new FileReader(kicadPcbFile)) {
            Board board = read(fileReader);
            board.setFile(kicadPcbFile.getCanonicalFile());
            return board;
        } catch (Exception e) {
            throw new RuntimeException("Failed to load "+kicadPcbFile, e);
        }
    }

    private static Board read(InputStreamReader fileReader) throws SexpParserException, IOException {
        return (Board) sexpToNode(SexpFactory.parse(fileReader));
    }

    private static Node sexpToNode(Sexp sexp) {
        RawNode rawNode = sexpToRawNode(sexp);

        if (rawNode.isKeyValue()) {
            return rawNode;
        }

        if (rawNode.getName().equals(Board.NAME)) {
            return new Board(rawNode);
        }

        if (rawNode.getName().equals(Module.NAME)) {
            return new Module(rawNode);
        }

        if (Position.isAtNode(rawNode)) {
            return new Position(rawNode);
        }

        if (rawNode.getName().equals(FpText.NAME)) {
            return new FpText(rawNode);
        }

        if (rawNode.getName().equals(Zone.NAME)) {
            return new Zone(rawNode);
        }

        if (rawNode.getName().equals(LineNode.NAME)) {
            return new LineNode(rawNode);
        }

        if (rawNode.getName().equals(Polygon.POLYGON) || rawNode.getName().equals(Polygon.FILLED_POLYGON)) {
            return new Polygon(rawNode);
        }

        return rawNode;
    }

    private static RawNode sexpToRawNode(Sexp sexp) {
        final List<NodeOrValue> children = new ArrayList<>();

        String name = null;
        for (Sexp ce : sexp) {
            if (ce instanceof SexpString) {
                if (name == null) {
                    name = ce.toString();
                } else {
                    children.add(new MutableValue(ce.toString()));
                }
            } else {
                if (name == null) {
                    throw new IllegalArgumentException("Did not find the name of the node before the first non-atomic child node: " + sexp.toIndentedString());
                }
                children.add(sexpToNode(ce));
            }
        }

        if (name == null) {
            throw new IllegalArgumentException("Did not find the name of the node: " + sexp.toIndentedString());
        }

        return new RawNode(name, children);
    }

    public static void write(Board board, Writer writer) throws IOException {
        writer.append(nodeToSexp(board).toIndentedString());
    }

    private static Sexp nodeToSexp(Node node) {
        Sexp res = SexpFactory.newNonAtomicSexp();

        res.add(SexpFactory.newAtomicSexp(node.getName()));

        for (NodeOrValue child : node.getAllChildren()) {
            if (child instanceof MutableValue) {
                res.add(SexpFactory.newAtomicSexp(((MutableValue) child).getValue()));
            } else if (child instanceof Node) {
                res.add(nodeToSexp((Node) child));
            }
        }

        return res;
    }

    public static <T extends Node> T clone(T input) {
        return (T)sexpToNode(nodeToSexp(input));
    }

    public static File storeWithBackup(Board board) throws IOException {
        File pcbFile = board.getFile();
        LocalDateTime now = LocalDateTime.now();
        String timestamp = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-MM-SS"));
        File backupDir = new File(pcbFile.getParentFile(), "backup");

        File backupPcbFile = new File(backupDir, "backup-"+timestamp+"-"+pcbFile.getName());

        if (!backupDir.isDirectory() && !backupDir.mkdir()) {
            FileUtils.forceMkdir(backupDir);
        }
        FileUtils.copyFile(pcbFile, backupPcbFile);

        try (Writer writer = new FileWriter(pcbFile)) {
            KicadPcbSerializer.write(board, writer);
        }

        return backupPcbFile;
    }
}
