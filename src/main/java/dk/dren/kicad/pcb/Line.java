package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.Point;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Line implements Comparable<Line> {
    private Point start;
    private Point end;

    public static Line of(RawNode segment) {
        Line line = new Line();
        line.start = ((Position)segment.getProperty(Position.START)).withoutRotation();
        line.end   = ((Position)segment.getProperty(Position.END)).withoutRotation();
        return line;
    }

    @Override
    public int compareTo(Line o) {
        int sc = start.compareTo(o.start);
        return sc != 0 ? sc : end.compareTo(o.end);
    }

    public Point getMidPoint() {
        return new Point(
                start.getX().add(end.getX()).divide(2),
                start.getY().add(end.getY()).divide(2)
                );
    }

    public Point intersection(Line other) {
        double x1 = start.getX().doubleValue();
        double y1 = start.getY().doubleValue();
        double x2 = end.getX().doubleValue();
        double y2 = end.getY().doubleValue();

        double x3 = other.getStart().getX().doubleValue();
        double y3 = other.getStart().getY().doubleValue();
        double x4 = other.getEnd().getX().doubleValue();
        double y4 = other.getEnd().getY().doubleValue();

        double denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);
        if (denom == 0.0) { // Lines are parallel.
            return null;
        }
        double ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3))/denom;
        double ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3))/denom;
        if (ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f) {
            // Get the intersection point.
            return new Point(
                    Decimal6f.valueOf(x1 + ua*(x2 - x1)),
                    Decimal6f.valueOf(y1 + ua*(y2 - y1)));
        }

        return null;
    }
    
    
    public Point _intersection(Line other) {
        Decimal6f s1x = end.getX().subtract(start.getX());
        Decimal6f s1y = end.getY().subtract(start.getY());
        Decimal6f s2x = other.getEnd().getX().subtract(other.getStart().getX());
        Decimal6f s2y = other.getEnd().getY().subtract(other.getStart().getY());

        Decimal6f divisor = s2x.negate().multiply(s1y).add(s1x.multiply(s2y));
        if (divisor.isZero()) {
            return null; // parallel
        }

        Decimal6f s = s1y.negate().multiply(start.getX().subtract(other.getStart().getX())).add(end.getX().multiply(start.getY().subtract(other.getStart().getY())))
                .divideBy(divisor);
        Decimal6f t = s2x.multiply(start.getY().subtract(other.getStart().getY()).subtract(s2y.multiply(start.getX().subtract(other.getStart().getX()))))
                .divideBy(divisor);

        if (s.isGreaterThanOrEqualTo(Decimal6f.ZERO) && s.isLessThanOrEqualTo(Decimal6f.ONE) &&
            t.isGreaterThanOrEqualTo(Decimal6f.ZERO) && t.isLessThanOrEqualTo(Decimal6f.ONE)) {

            return new Point(
                    start.getX().add(t.multiply(s1x)),
                    start.getY().add(t.multiply(s1y))
            );
        }

        return null; // No collision
    }

    public Vector vector() {
        return new Vector(end.getX().subtract(start.getX()),  end.getY().subtract(start.getY()));
    }

    public Collection<NodeOrValue> asNodes() {
        ArrayList<NodeOrValue> n = new ArrayList<>();
        n.add(new RawNode(Position.START, new MutableValue(start.getX()), new MutableValue(start.getY())));
        n.add(new RawNode(Position.END,   new MutableValue(end.getX()),   new MutableValue(end.getY())));
        return n;
    }

    public List<Point> intersection(Point center, Decimal6f radiusf) {
        double radius = radiusf.doubleValue();
        double baX = end.getX().doubleValue() - start.getX().doubleValue();
        double baY = end.getY().doubleValue() - start.getY().doubleValue();
        double caX = center.getX().doubleValue() - start.getX().doubleValue();
        double caY = center.getY().doubleValue() - start.getY().doubleValue();

        double a = baX * baX + baY * baY;
        double bBy2 = baX * caX + baY * caY;
        double c = caX * caX + caY * caY - radius * radius;

        double pBy2 = bBy2 / a;
        double q = c / a;

        double disc = pBy2 * pBy2 - q;
        if (disc < 0) {
            return Collections.emptyList();
        }
        // if disc == 0 ... dealt with later
        double tmpSqrt = Math.sqrt(disc);
        double abScalingFactor1 = -pBy2 + tmpSqrt;
        double abScalingFactor2 = -pBy2 - tmpSqrt;

        Point p1 = new Point(start.getX().doubleValue() - baX * abScalingFactor1,
                             start.getY().doubleValue() - baY * abScalingFactor1);
        if (disc == 0) { // abScalingFactor1 == abScalingFactor2
            return Collections.singletonList(p1);
        }
        Point p2 = new Point(start.getX().doubleValue() - baX * abScalingFactor2,
                             start.getY().doubleValue() - baY * abScalingFactor2);

        return Stream.of(p1, p2)
                .filter(this::contains)
                .collect(Collectors.toList());
    }

    /**
     * This is slightly tricky, it treats the line as a bounding box and checks if the point is inside the bounding box
     * this can be used to find out if a point which is known to be on the line is in the line segment.
     */
    private boolean contains(Point p) {

        Decimal6f minX = start.getX().min(end.getX());
        Decimal6f minY = start.getY().min(end.getY());
        Decimal6f maxX = start.getX().max(end.getX());
        Decimal6f maxY = start.getY().max(end.getY());

        return  p.getX().isGreaterThanOrEqualTo(minX) &&
                p.getX().isLessThanOrEqualTo(maxX) &&
                p.getY().isGreaterThanOrEqualTo(minY) &&
                p.getY().isLessThanOrEqualTo(maxY);
    }
}
